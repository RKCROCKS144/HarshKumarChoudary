<p align = "right">
  <img src="https://profile-counter.glitch.me//HarshKumarChoudary/count.svg" />
</p>

<p>
  <img align="center" src="https://readme-typing-svg.herokuapp.com?font=Playfair+Display&color=F70000&size=30&center=true&vCenter=true&multiline=true&weight=100&height=100&width=220&lines=Hey+there%2C;I'm+Harsh!">
  <img align = "left" src='https://raw.githubusercontent.com/ShahriarShafin/ShahriarShafin/main/Assets/handshake.gif' width="70px"> 
</p>

<br>
<img align='right' src="https://raw.githubusercontent.com/Suvraneel/Suvraneel/master/res/readme_banner.gif" width="260" height="auto">
<br>

- :octocat: &nbsp;[![Resume](https://img.shields.io/badge/HarshKumarChoudhary-RESUME-blue?style=for-the-badge&logo=Sega)](https://github.com/HarshKumarChoudary/HarshKumarChoudary/blob/main/Harsh's%20Resume.pdf)
- :computer: &nbsp;I’m currently pursuing B.Tech in **Information Technology** at the  
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**[National Institute Of Technology Raipur](http://www.nitrr.ac.in/)**
- :crystal_ball: &nbsp;I’m currently working on Responsive Web Design, JavaScript, SQL
- :mailbox: &nbsp;You can reach out to me at : ***harshkumarchoudhary144@gmail.com***
- :bowtie: &nbsp;Pronouns: He/Him
- :video_game: &nbsp;**Hobbies**: PC Gaming, Coding, Reading, Soccer

#
<br>
<h2 align=left>
<img src="https://raw.githubusercontent.com/Suvraneel/Suvraneel/master/res/social.gif" height="35" width= auto>
&nbsp;&nbsp;&nbsp;&nbsp;
Connect with me on:
<br></h2>


<!-- 
[![GitHub-Mark-Light](https://raw.githubusercontent.com/Suvraneel/Suvraneel/master/res/in.png#gh-light-mode-only)![GitHub-Mark-Dark](https://raw.githubusercontent.com/Suvraneel/Suvraneel/master/res/in.png#gh-dark-mode-only)](https://www.linkedin.com/in/suvraneel-bhuin) -->


[![LinkedIn](https://raw.githubusercontent.com/Suvraneel/Suvraneel/master/res/in.png#gh-light-mode-only)](https://www.linkedin.com/in/harsh-kumar-choudhary-64228118b/) &nbsp;&nbsp;
[![Discord](https://raw.githubusercontent.com/Suvraneel/Suvraneel/master/res/dc.jpg#gh-light-mode-only)](https://discord.com/users/HARSH+KUMAR+CHOUDHARY#2570) &nbsp;&nbsp;
[![WhatsApp](https://raw.githubusercontent.com/Suvraneel/Suvraneel/master/res/wp.png#gh-light-mode-only)](https://api.whatsapp.com/send?phone=7999930798&text=Hi!%20Harsh!!) &nbsp;&nbsp;


<br>
<h2 align=left>
<img src="https://raw.githubusercontent.com/Suvraneel/Suvraneel/master/res/target.gif" height="40" width= auto>
&nbsp;&nbsp;&nbsp;&nbsp;
Find me on Coding Platforms:
<br></h2>


[![LeetCode](https://img.shields.io/badge/-LeetCode-da8200?style=for-the-badge&logo=LeetCode&logoColor=ffa116&labelColor=black)](https://leetcode.com/HarshKumarChoudhary/)
[![CodeChef](https://img.shields.io/badge/Codechef-372a22?&style=for-the-badge&logo=Codechef&logoColor=red&labelColor=black)](https://www.codechef.com/users/rkcrocks144)
[![HackerRank](https://img.shields.io/badge/-Hackerrank-00c353?style=for-the-badge&logo=HackerRank&logoColor=00EA64&labelColor=black)](https://www.hackerrank.com/harsh_K_C)
[![CodeForces](https://img.shields.io/badge/-Codeforces-131342?style=for-the-badge&logo=Codeforces&logoColor=white&labelColor=0A0A23)](https://codeforces.com/profile/RKCROCKS144)
[![GFG](https://img.shields.io/badge/GeeksforGeeks-298D46?style=for-the-badge&logo=geeksforgeeks&logoColor=4dcb72&labelColor=black)](https://auth.geeksforgeeks.org/user/harshkumarchoudhary144/profile)
[![Dare2Compete](https://img.shields.io/badge/-Dare2compete-da8200?style=for-the-badge&logo=Dare2Compete&logoColor=ffa116&labelColor=black)](https://dare2compete.com/user/profile)

##


<br>
<h2 align=left>
<img src="https://raw.githubusercontent.com/Suvraneel/Suvraneel/master/res/ufo.gif" height="50" width= auto>
&nbsp;&nbsp;&nbsp;&nbsp;
Competencies:
<br></h2>


<table>
<tr>
<td><h4>Programming Languages</h4></td>
<td><img src="https://img.shields.io/badge/CPP-blue?style=for-the-badge&logo=cplusplus&logoColor=blue&color=00599C&labelColor=black"/> 
<img src="https://img.shields.io/badge/C-black?style=for-the-badge&logo=c&labelColor=black&color=404040" />
<img src="https://img.shields.io/badge/Java-orange?style=for-the-badge&logo=java&logoColor=ff7019&labelColor=141819&color=ff7019"/>
<img src="https://img.shields.io/badge/Python-blue?style=for-the-badge&logo=python&labelColor=black&color=3776ab" />
<img src="https://img.shields.io/badge/Javascript-yellow?style=for-the-badge&logo=javascript&labelColor=black&color=DFA200" /></td></tr>

<tr>
<td><h4>Web Development</h4></td>
<td><img src="https://img.shields.io/badge/HTML5-red?style=for-the-badge&logo=html5&labelColor=black&color=E34F26"/>
<img src="https://img.shields.io/badge/CSS3-white?style=for-the-badge&logo=css3&logoColor=1572B6&labelColor=black&color=1572B6" />
<img src="https://img.shields.io/badge/Bootstrap-purple?style=for-the-badge&logo=bootstrap&labelColor=black&color=7952B3"/>
<img src="https://img.shields.io/badge/Javascript-yellow?style=for-the-badge&logo=javascript&labelColor=black&color=c89100"/>
<img src="https://img.shields.io/badge/MongoDB-green?style=for-the-badge&logo=mongodb&labelColor=black&color=409040"/>
<img src="https://img.shields.io/badge/Express-black?style=for-the-badge&logo=express&labelColor=black&color=1f1f1f"/>
<img src="https://img.shields.io/badge/React-blue?style=for-the-badge&logo=react&labelColor=black&color=3a8296"/>
<img src="https://img.shields.io/badge/Node.JS-blue?style=for-the-badge&logo=node.js&logoColor=lime&labelColor=black&color=236b23"/>
<img src="https://img.shields.io/badge/Postman-orange?style=for-the-badge&logo=postman&labelColor=black&color=ff4704"/></td></tr>

<tr>
<td><h4>App Development</h4></td>
<td><img src="https://img.shields.io/badge/Flutter-0a97c2?style=for-the-badge&logo=flutter&logoColor=0dbdf2&labelColor=black&color=0ba0cd"/>
<img src="https://img.shields.io/badge/Dart-blue?style=for-the-badge&logo=dart&logoColor=2eb8b8&labelColor=black&color=269999"/>
<img src="https://img.shields.io/badge/Android%20Studio-green?style=for-the-badge&logo=android%20studio&labelColor=black&color=2a9a5c"/></td></tr>

<tr>
<td><h4>Tools & Project Management</h4></td>
<td><img src="https://img.shields.io/badge/Git-red?style=for-the-badge&logo=git&labelColor=black&color=red"/>  
<img src="https://img.shields.io/badge/GitHub-black?style=for-the-badge&logo=github&labelColor=black&color=181717"/>  
<img src="https://img.shields.io/badge/VSCode-cyan?style=for-the-badge&logo=visual%20studio%20code&labelColor=00497a&color=007ACC"/>  
<img src="https://img.shields.io/badge/Repl.it-black?style=for-the-badge&logo=replit&labelColor=black&color=1e2426"/>  
<img src="https://img.shields.io/badge/Eclipse%20IDE-purple?style=for-the-badge&logo=eclipse%20IDE&labelColor=1a1433&color=2C2255"/>  
<img src="https://img.shields.io/badge/Codepen-black?style=for-the-badge&logo=codepen&labelColor=black&color=141819"/>
<img src="https://img.shields.io/badge/Heroku-180036?style=for-the-badge&logo=heroku&labelColor=180036&color=4300
  </tr>  

<tr>
<td><h4>Machine/Deep Learning</h4></td>
<td><img src="https://img.shields.io/badge/Pandas-black?style=for-the-badge&logo=pandas&labelColor=0c0234&color=150458"/>  
<img src="https://img.shields.io/badge/NumPy-blue?style=for-the-badge&logo=numpy&labelColor=001921&color=013243"/>  
<img src="https://img.shields.io/badge/TensorFlow-black?style=for-the-badge&logo=tensorflow&labelColor=141819&color=FF6F00"/>
<img src="https://img.shields.io/badge/Skikit%20Learn-orange?style=for-the-badge&logo=scikit%2Dlearn&labelColor=141819&color=F7931E"/>  
<img src="https://img.shields.io/badge/Keras-black?style=for-the-badge&logo=keras&labelColor=680000&color=D00000"/></td></tr>

<tr>
<td><h4>Miscellaneous</h4></td>
<td><img src="https://img.shields.io/badge/Arduino-blue?style=for-the-badge&logo=arduino&labelColor=black&color=00979D"/><img src="https://img.shields.io/badge/VHDL-cc0000?style=for-the-badge&logo=xilinx&logoColor=cc0000&labelColor=black&color=cc0000"/><img src="https://img.shields.io/badge/GNU_Bash-blue?style=for-the-badge&logo=gnubash&labelColor=black&color=4EAA25"/></td></tr>
</table>



<!-- <br>
<h2 align=left>
<img src="https://raw.githubusercontent.com/Suvraneel/Suvraneel/master/res/laptop.gif" height="50" width= auto>
&nbsp;&nbsp;&nbsp;&nbsp;
Github Stats :
<br></h2> -->


<hr>


<!--START_SECTION:activity-->


---
[linkedin]:https://www.linkedin.com/in/harsh-kumar-choudhary-64228118b/











































<!---
HarshKumarChoudary/HarshKumarChoudary is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a looat your changes.
--->
